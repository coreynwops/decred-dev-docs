#!/usr/bin/env bash
docker run -ti --rm -v ${PWD}:/app --entrypoint=/go/bin/liche nwops/liche -v content/**/*.md layouts/**/*.html