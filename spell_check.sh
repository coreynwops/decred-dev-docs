docker run -ti -v $(pwd):/workdir tmaier/markdown-spellcheck:latest \
"content/**/*.md" --ignore-acronyms --ignore-numbers --en-us
