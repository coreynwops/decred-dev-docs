<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Dev Docs](#dev-docs)
  - [Getting started with doc development](#getting-started-with-doc-development)
  - [Update the theme](#update-the-theme)
  - [Layout](#layout)
    - [Global Menu](#global-menu)
    - [Page Menu](#page-menu)
  - [Generating new doc sections](#generating-new-doc-sections)
  - [Viewing the docs](#viewing-the-docs)
  - [Running the grammar check](#running-the-grammar-check)
  - [Running the link checker](#running-the-link-checker)
  - [Running the spell checker](#running-the-spell-checker)
    - [Add new words to the spell checker dictionary](#add-new-words-to-the-spell-checker-dictionary)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Dev Docs

The dev docs are managed by a static site generator called [Hugo](https://gohugo.io/).

Working with hugo requires you to learn the hugo generator system.
When creating more docs you can follow the [hugo docs](https://gohugo.io/documentation//) for more information.

## Getting started with doc development

In order to get started with writing docs or updating the site navigation you first need to clone this repo and perform the following steps.

0. [install hugo](https://gohugo.io/getting-started/installing/)
1. `git clone https://gitlab.com/coreynwops/decred-dev-docs`
1. `bash ./pull-theme.sh`
1. `cd decred-dev-docs`
1. `hugo`

If successful you should see:

```
 ~/decred/decred-dev-docs$ hugo

                   | EN
+------------------+----+
  Pages            | 76
  Paginator pages  |  0
  Non-page files   |  0
  Static files     | 93
  Processed images |  0
  Aliases          | 34
  Sitemaps         |  1
  Cleaned          |  0

Total in 86 ms
```

## Update the theme

There will be a time when the hugo theme requires an update. In order to update you
will need to run the following command:

`bash ./pull-theme.sh`

You can perform this at any time.

## Layout

### Global Menu

The docs are layed out with a few different menus. First, there is the top nav bar menu. Some of the links that exist there are just placeholders so if something needs to be added or removed. This menu is considered the global menu, as noted in the `config.toml` file with `[[menu.global]]`

### Page Menu

Each main page of the global menu has its own menu system as well. Btw, this is often referred as the taxonomy. As an example you can see the main menu system here:

```
[[menu.docs]]
  name = "Getting Started"
  weight = 1
  identifier = "getting-started"
  url = "/getting-started/"
```

In this example the menu.docs site menu has a link to the getting-started page.

These docs are all stored under the content directory.

## Generating new doc sections

I have created am archetype for generating a new doc. Essentially, this is just a template so we can easily create or modify archetypes.

`hugo new -k doc_section en/<topic>/<subtopic>/_index.md`

ie. `hugo new -k doc_section en/communication/irc/_index.md`

So when you want to create a new topic this is all that is required. After you create this file, you just need to add the markdown content in the `_index.md` file.

Also keep in mind there are some cool snippets you can use in your content. Please see
the [shortcodes](https://gohugo.io/content-management/shortcodes/) for more info

## Viewing the docs

You can run the hugo site generator at any time to ensure your docs do not contain errors.
`hugo`

To see the docs website you can run `hugo serve` which generates the docs and starts a webserver. This is also a good way to develop too since hugo adds file watchers and re-generates the static content as you modify it. This is all done live so no need to restart `hugo serve`.

```
  ~/decred/decred-dev-docs   master ●  hugo serve

                   | EN
+------------------+----+
  Pages            | 76
  Paginator pages  |  0
  Non-page files   |  0
  Static files     | 93
  Processed images |  0
  Aliases          | 34
  Sitemaps         |  1
  Cleaned          |  0

Total in 44 ms
Watching for changes in /decred/decred-dev-docs/{content,data,themes}
Watching for config changes in /decred/decred-dev-docs/config.toml
Serving pages from memory
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at http://localhost:1313/decred-dev-docs/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```

## Running the grammar check

To run the grammar check against your docs you can utilize the following docker command.

`docker run -it --rm -v $(pwd):/app nwops/write-good content/**/*.md`

or

`bash ./grammar_check.sh`

**NOTE** You need to be in the root directory to run this.

## Running the link checker

To run the link checker against your docs you can utilize the following docker command.

`docker run -ti --rm -v ${PWD}:/app --entrypoint=/go/bin/liche nwops/liche content/**/*.md`

or

`bash ./link_checker.sh`

## Running the spell checker

To run the spell checker against your docs you can utilize the following docker command.

These command should be run from the root of the repository.

```
docker run -ti -v $(pwd):/workdir tmaier/markdown-spellcheck:latest \
"content/**/*.md" --ignore-acronyms --ignore-numbers --en-us
```

or

```
bash spell_check.sh
```

### Add new words to the spell checker dictionary

Simply just add the desired word to the .spelling file in the root of this repository.
