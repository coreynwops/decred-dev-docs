---
title: "{{ replace .Name "-" " " | title }}"
linktitle: "{{ replace .Name "-" " " | title }}"
description: Some Description
date: {{ .Date }}
publishdate: {{ .Date }}
lastmod: {{ .Date }}
categories: []
keywords: []
menu:
  docs:
    parent: "{{ replace .Name "-" " " | title }}"
    weight: 2
weight: 1
draft: false
aliases: [/{{ replace .Name "-" " " | title }}/]
toc: false
---
