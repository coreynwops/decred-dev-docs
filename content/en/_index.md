---
title: "The world’s fastest docs"
date: 2017-03-02T12:00:00-05:00s
features:
  - heading: Quick Start
    image_path: /images/icon-fast.svg
    tagline: Want to get started quickly, use the quick start
    copy: The quick start is designed to get your development environment setup quickly.

  - heading: Need a deep dive into the docs?
    image_path: /images/icon-content-management.svg
    tagline: Read some of the more advanced topics on Decred
    copy: If you are developing a new feature and need help with some in-depth technical information.  Look no further.

sections:
  - heading: "100s of Themes"
    cta: Check out the Hugo themes.
    link: http://themes.gohugo.io/
    color_classes: bg-accent-color white
    image: /images/homepage-screenshot-hugo-themes.jpg
    copy: "Hugo provides a robust theming system that is easy to implement but capable of producing even the most complicated websites."
  - heading: "Capable Templating"
    cta: Get Started.
    link: templates/
    color_classes: bg-primary-color-light black
    image: /images/home-page-templating-example.png
    copy: "Hugo's Go-based templating provides just the right amount of logic to build anything from the simple to complex. If you prefer Jade/Pug-like syntax, you can also use Amber, Ace, or any combination of the three."
---
