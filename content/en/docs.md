---
title: Decred Developer Documentation
linktitle: Hugo
description: Developer documentation when contributing to the Decred Project
date: 2017-02-01
publishdate: 2017-02-01

weight: 01 #rem
draft: false
? slug
aliases: []
toc: false
---

Below you will find some of the most common and helpful pages from our documentation.
