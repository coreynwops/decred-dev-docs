---
title: Getting Started
linktitle: Overview
description: How to start developing
date: 2018-08-13
publishdate: 2018-08-13
lastmod: 2018-08-13
categories: []
keywords: []
menu:
  docs:
    parent: "getting-started"
    weight: 1
weight: 1
draft: false
aliases: [/getting-started/]
toc: true
---

If this is your first time developing Decred and you've [already installed dcrd on your machine][installed], we recommend the [quick start][].

[installed]: /getting-started/installing/
[quick start]: /getting-started/getting-started/
