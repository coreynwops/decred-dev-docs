---
title: "Picking a project"
linktitle: "Picking a project"
description: Some Description
date: 2018-08-16T11:54:38-07:00
publishdate: 2018-08-16T11:54:38-07:00
lastmod: 2018-08-16T11:54:38-07:00
categories: []
keywords: []
menu:
  docs:
    parent: "getting-started"
    weight: 3
weight: 1
draft: false
aliases: [/Picking_a_project/]
toc: false
---

There are many projects within the Decred ecosystem. It can be daunting at first because you may not know where to start. However, knowledge of the overall architecture will you help understand where all the pieces fall into play.

## DCRD

First off dcrd is the main dependency of almost every project. Without dcrd nothing really else matters as most other projects are built on top of dcrd to provide some additional functionality. DCRD is the blood of the Decred ecosystem.

You will need to have Dcrd available in one form or another to work on the other projects.

## DCRData

Dcrdata builds o top DCRD and provides a way to query transactions and block information. This is consider the block explorer. This is just a standard database backed, web frontend stack with a Go/Nginx webserver in the middle serving up users cached content.

Why Go? Go is wicked fast and has concurrency at the core. Why Nginx? Nginx provides a reverse proxy and content caching strategies that Go is not suitable for.

To get started with Dcrdata you need postgres, dcrd, dcrdata and nginx. Docker support coming soon.

## Decredition

This is the user facing wallet application. This is a electron based application which is cross platform. Electron is a way to use web technologies that embeds chrome browser engine
to render content. So you get to use your favorite web syntax to build native apps. The Decredition app uses react/redux internally.

## Mobile Apps

### Android App

https://github.com/decred/dcrandroid

### IOS

https://github.com/raedahgroup/dcrios
