---
title: "Join our Slack community"
linktitle: "Slack"
description: Some Description
date: 2018-08-16T13:38:52-07:00
publishdate: 2018-08-16T13:38:52-07:00
lastmod: 2018-08-16T13:38:52-07:00
categories: []
keywords: []
menu:
  docs:
    parent: "communication"
    weight: 6
weight: 1
draft: false
aliases: [/Slack/]
toc: false
---

Slack is one of several Decred community platforms, connecting the people in real time. The growing community has channels for a number of matters such as:

- support
- development
- privacy
- smart contracts
- mining
- trading
- marketing
- design
- country-specific channels
- and much more

5000+ members bring together expertise and know-how consisting of developers, marketers, designers, writers and other contributors hang out here and answer questions.

[Join the Decred Slack Group Here](https://slack.decred.org/)

## Popular Slack Channels

### Dev

Join the dev channel for questions around development.

### Documentation
