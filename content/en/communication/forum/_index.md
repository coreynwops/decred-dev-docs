---
title: "Forum"
linktitle: "Forum"
description: Some Description
date: 2018-08-16T15:25:43-07:00
publishdate: 2018-08-16T15:25:43-07:00
lastmod: 2018-08-16T15:25:43-07:00
categories: []
keywords: []
menu:
  docs:
    parent: "communication"
    weight: 2
weight: 1
draft: false
aliases: [/Forum/]
toc: false
---

https://forum.decred.org/
