---
title: "Code_changes"
linktitle: "Code_changes"
description: Some Description
date: 2018-08-23T09:01:12-07:00
publishdate: 2018-08-23T09:01:12-07:00
lastmod: 2018-08-23T09:01:12-07:00
categories: []
keywords: []
menu:
  docs:
    parent: "code_reviews"
    weight: 2
weight: 1
draft: false
aliases: [/Code_changes/]
toc: false
---

After a PR (Pull Request)
Being able to click the View Changes button, or just clicking the new commit, is really helpful so that the reviewer does not have to re-review the entire PR again, just the incremental changes.
After changes are "approved" on github, it's OK to squash and rebase, but in general I don't care all that much because github has a Squash and Merge button. In this case some text for the squash commit is appreciated, but I often write it myself or fuse the individual commit messages.
